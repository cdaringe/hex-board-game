# hex-board-game

the current suite of _things_ is for prepping laser cut & engraving of:

- board tiles
- board numbers

## todo

- game container
  - ports

## how-to

- https://cdaringe.github.io/tessl
- export a svg
  - amend the downloaded svg by re-adding `xlink:` prefixes in front of `href`s
- in inkscape@1.x or _later_
  - join all paths as described [here](https://gitlab.com/cdaringe/hex-board-game)
- embed images using inkscape patterns (Object => Patterns => Object to pattern), then using pattern fills in path "Fill & stroke"

arrange. cut.
